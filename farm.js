/*
javascript:
script = document.createElement('script');
script.type = 'text/javascript';
script.src = 'https://dl.dropboxusercontent.com/s/3qg4rrxbe0hsuyw/farm.js';
document.getElementsByTagName("head")[0].appendChild(script);
void( 0 );
*/

var win=window;
//var win=(window.frames.length > 0)?window.main:window;
if (document.URL.search(/screen=place/)!= -1 && document.URL.search(/try=confirm/)===-1) {
	var units=new Array($( "[id^=unit_input_]").length);
	for (i =0; i<$( "[id^=unit_input_]").length;i++){
		units[i]=parseInt((localStorage.getItem('farm_unit_'+i))?localStorage.getItem('farm_unit_'+i):0)
	}
	var farmIndex=parseInt((localStorage.getItem('farm_Index'))?localStorage.getItem('farm_Index'):0);
	var coords=(localStorage.getItem('farm_coords'))?localStorage.getItem('farm_coords'):'555|555 444|444';
	
	if(!document.getElementById("akk_msg")){
		var name='Фармскрипт';
		var version='2.0';
		var site='https://akkela.net/%D1%81%D0%BA%D1%80%D0%B8%D0%BF%D1%82-%D1%84%D0%B0%D1%80%D0%BC/';
		var forum='https://forum.voyna-plemyon.ru/index.php?threads/30120';
		var akkRight=-140;
		var data="";
		var divNode = document.createElement('div');
		divNode.id='akk_msg';
		data='<img style = "border-radius: 15px 0px 0px 15px;float:left; margin-right:5px" src="https://dl.dropboxusercontent.com/s/de0p634sfrsf8u9/logo100.JPG" height="80" width="75"></div width="'+(-akkRight)+'">'+name+'<br />Версия: '+version+'<br /><a target="_blank" href="'+site+'">Страница скрипта</a><br /><a target="_blank" href="'+forum+'">Форум игры</a></div>';
		divNode.innerHTML = data;
		divNode.style.zIndex="999999"
		divNode.style.position="fixed";
		divNode.style.display="block";
		divNode.style.top="0px";
		divNode.style.right=akkRight+"px";
		divNode.style.backgroundColor="orange";
		divNode.style.borderRadius="15px 0px 0px 15px";
		divNode.style.minWidth=(70-akkRight)+'px';
		divNode.onmouseover=function(){this.style.right="0"};
		divNode.onmouseout=function(){this.style.right=akkRight+"px"};
		document.body.appendChild(divNode);
		
		var divNode2 = document.createElement('div');
		divNode2.id='pushfarmCoords';
		data='<textarea id="farm_coords" style="resize:vertical; width:100%" placeholder="Координаты целевых деревень" rows="5" resize="none" onFocus="this.select();"/>'+coords+'</textarea><br/><div style = "width:100%">Kоордината номер: <input id="farmIndex" type="text" style="width: 20px" value="'+(farmIndex+1)+'"> из <span id="coordsLength" value="'+coords.split(" ").length+'"></span> <input readonly onClick = "setLS()" class="btn" value="Сохранить для скрипта"></div>';
		divNode2.innerHTML = data;
		$('.vis.modemenu')[0].outerHTML=$('.vis.modemenu')[0].outerHTML+divNode2.outerHTML;
	}
	function cleanCoords(){
		var matched=document.getElementById('farm_coords').value.match(/[0-9]{3}\|[0-9]{3}/g);
		var output='';
		if(matched.length>0){
			output=matched[0];
			for (i=1;i<matched.length;i++){
				output=output + ' ' + matched[i];
			}
		} 
		document.getElementById('farm_coords').value=output;
		document.getElementById('coordsLength').innerHTML=document.getElementById('farm_coords').value.split(" ").length;
	}
	function setLS(){
		for (i =0; i<$( "[id^=unit_input_]").length;i++){
			localStorage.setItem('farm_unit_'+i, ($( "[id^=unit_input_]")[i].value!='')?$( "[id^=unit_input_]")[i].value:0);
		}
		localStorage.setItem('farm_Index', parseInt(document.getElementById('farmIndex').value)-1);
		cleanCoords();
		localStorage.setItem('farm_coords', document.getElementById('farm_coords').value);
		alert("Сохранил новые данные");
	}
	for (i =0; i<$( "[id^=unit_input_]").length;i++){
			$( "[id^=unit_input_]")[i].value=units[i];
	}
	coords = coords.split(" ");
	document.getElementById('coordsLength').innerHTML=coords.length;
	if (farmIndex >= coords.length) {
		farmIndex = 0;
		document.getElementById('farmIndex').value=1;	
	}
	coord = coords[farmIndex];
	coord = coord.split("|");
	farmIndex = farmIndex + 1;
	document.getElementById('farmIndex').value=farmIndex;
	localStorage.setItem('farm_Index', farmIndex);
	document.forms[0].x.value = coord[0];
	document.forms[0].y.value = coord[1];
	document.forms[0].y.focus();
} else {
	alert("Этот скрипт работает только на площади. Переход...");
	self.location = win.game_data.link_base_pure.replace(/screen\=\w*/i, "screen=place");
}
void(0);